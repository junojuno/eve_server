-- MySQL dump 10.13  Distrib 5.5.44, for debian-linux-gnu (x86_64)
--
-- Host: localhost    Database: eve
-- ------------------------------------------------------
-- Server version	5.5.44-0ubuntu0.12.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_group_permission_group_id_689710a9a73b7457_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  CONSTRAINT `auth__content_type_id_508cf46651277a81_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add log entry',1,'add_logentry'),(2,'Can change log entry',1,'change_logentry'),(3,'Can delete log entry',1,'delete_logentry'),(4,'Can add permission',2,'add_permission'),(5,'Can change permission',2,'change_permission'),(6,'Can delete permission',2,'delete_permission'),(7,'Can add group',3,'add_group'),(8,'Can change group',3,'change_group'),(9,'Can delete group',3,'delete_group'),(10,'Can add user',4,'add_user'),(11,'Can change user',4,'change_user'),(12,'Can delete user',4,'delete_user'),(13,'Can add content type',5,'add_contenttype'),(14,'Can change content type',5,'change_contenttype'),(15,'Can delete content type',5,'delete_contenttype'),(16,'Can add session',6,'add_session'),(17,'Can change session',6,'change_session'),(18,'Can delete session',6,'delete_session'),(19,'Can add image file',7,'add_imagefile'),(20,'Can change image file',7,'change_imagefile'),(21,'Can delete image file',7,'delete_imagefile'),(22,'Can add user',8,'add_user'),(23,'Can change user',8,'change_user'),(24,'Can delete user',8,'delete_user'),(25,'Can add alarm',9,'add_alarm'),(26,'Can change alarm',9,'change_alarm'),(27,'Can delete alarm',9,'delete_alarm'),(28,'Can add Device',10,'add_device'),(29,'Can change Device',10,'change_device'),(30,'Can delete Device',10,'delete_device');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_u_permission_id_384b62483d7071f0_fk_auth_permission_id` (`permission_id`),
  CONSTRAINT `auth_user_u_permission_id_384b62483d7071f0_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  CONSTRAINT `auth_user_user_permissi_user_id_7f0938558328534a_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `djang_content_type_id_697914295151027a_fk_django_content_type_id` (`content_type_id`),
  KEY `django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id` (`user_id`),
  CONSTRAINT `django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `djang_content_type_id_697914295151027a_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_45f3b1d93ec8c61c_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'admin','logentry'),(3,'auth','group'),(2,'auth','permission'),(4,'auth','user'),(5,'contenttypes','contenttype'),(9,'eve','alarm'),(7,'eve','imagefile'),(8,'eve','user'),(10,'gcm','device'),(6,'sessions','session');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2015-08-19 20:57:54'),(2,'auth','0001_initial','2015-08-19 20:57:54'),(3,'admin','0001_initial','2015-08-19 20:57:55'),(4,'contenttypes','0002_remove_content_type_name','2015-08-19 20:57:55'),(5,'auth','0002_alter_permission_name_max_length','2015-08-19 20:57:55'),(6,'auth','0003_alter_user_email_max_length','2015-08-19 20:57:55'),(7,'auth','0004_alter_user_username_opts','2015-08-19 20:57:55'),(8,'auth','0005_alter_user_last_login_null','2015-08-19 20:57:55'),(9,'auth','0006_require_contenttypes_0002','2015-08-19 20:57:55'),(10,'gcm','0001_initial','2015-08-19 20:57:55'),(11,'eve','0001_initial','2015-08-19 20:57:55'),(12,'eve','0002_auto_20150729_0212','2015-08-19 20:57:55'),(13,'eve','0003_auto_20150729_0217','2015-08-19 20:57:55'),(14,'eve','0004_user_device','2015-08-19 20:57:55'),(15,'eve','0005_auto_20150804_0318','2015-08-19 20:57:55'),(16,'eve','0006_auto_20150808_2355','2015-08-19 20:57:55'),(17,'eve','0007_user_picture','2015-08-19 20:57:55'),(18,'eve','0008_alarm_isvibrate','2015-08-19 20:57:55'),(19,'eve','0009_auto_20150812_0359','2015-08-19 20:57:55'),(20,'eve','0010_auto_20150814_1336','2015-08-19 20:57:56'),(21,'sessions','0001_initial','2015-08-19 20:57:56');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eve_alarm`
--

DROP TABLE IF EXISTS `eve_alarm`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eve_alarm` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `level` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  `end_time` datetime DEFAULT NULL,
  `start_time` datetime DEFAULT NULL,
  `stat` int(11) NOT NULL,
  `is_vibrate` int(11) NOT NULL,
  `first_img_id` int(11) DEFAULT NULL,
  `last_img_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `eve_alarm_e8701ad4` (`user_id`),
  KEY `eve_alarm_1bfa1b45` (`first_img_id`),
  KEY `eve_alarm_38ad4ba1` (`last_img_id`),
  CONSTRAINT `eve_alarm_last_img_id_79a02b3913af21fd_fk_eve_imagefile_id` FOREIGN KEY (`last_img_id`) REFERENCES `eve_imagefile` (`id`),
  CONSTRAINT `eve_alarm_first_img_id_43a73c70ef2cc6d4_fk_eve_imagefile_id` FOREIGN KEY (`first_img_id`) REFERENCES `eve_imagefile` (`id`),
  CONSTRAINT `eve_alarm_user_id_368a7125f7742eb2_fk_eve_user_id` FOREIGN KEY (`user_id`) REFERENCES `eve_user` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=227 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eve_alarm`
--

LOCK TABLES `eve_alarm` WRITE;
/*!40000 ALTER TABLE `eve_alarm` DISABLE KEYS */;
INSERT INTO `eve_alarm` VALUES (1,1,1,'2015-08-15 06:26:35','2015-08-19 21:10:13',1,0,2,315),(2,1,1,'2015-08-15 09:11:06','2015-08-19 21:10:59',1,0,3,316),(3,2,1,'2015-08-15 13:59:21','2015-08-19 21:11:14',1,0,5,317),(4,3,1,'2015-08-15 16:28:35','2015-08-19 21:11:29',1,0,1,318),(5,2,1,'2015-08-16 03:06:19','2015-08-19 21:12:11',1,0,5,319),(6,3,1,'2015-08-16 11:12:57','2015-08-19 20:12:45',1,0,1,320),(7,4,1,'2015-08-16 19:45:12','2015-08-19 20:13:04',1,0,3,321),(8,1,1,'2015-08-16 20:03:12','2015-08-19 20:13:12',1,0,1,322),(9,3,1,'2015-08-16 22:13:12','2015-08-19 20:13:12',1,0,4,323),(10,3,1,'2015-08-17 08:53:47','2015-08-19 21:53:28',1,0,3,324),(12,4,1,'2015-08-17 14:27:01','2015-08-19 22:42:11',1,0,3,325),(13,5,1,'2015-08-17 19:45:01','2015-08-19 22:42:11',1,1,1,326),(14,1,1,'2015-08-18 11:21:01','2015-08-19 22:42:11',1,0,4,327),(15,2,1,'2015-08-18 15:27:01','2015-08-19 22:42:11',1,0,5,328),(16,4,1,'2015-08-18 19:51:01','2015-08-19 22:42:11',1,0,4,329),(17,4,1,'2015-08-19 12:27:01','2015-08-19 22:42:11',1,0,2,330),(18,5,1,'2015-08-19 19:01:01','2015-08-19 22:42:11',1,1,3,331),(19,1,1,'2015-08-20 01:01:01','2015-08-20 22:42:11',1,0,1,332),(20,3,1,'2015-08-20 21:27:01','2015-08-20 22:42:11',1,0,3,333),(21,5,1,'2015-08-20 19:19:23','2015-08-20 22:00:00',1,1,1,334);
/*!40000 ALTER TABLE `eve_alarm` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eve_imagefile`
--

DROP TABLE IF EXISTS `eve_imagefile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eve_imagefile` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `img` varchar(100) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=353 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eve_imagefile`
--

LOCK TABLES `eve_imagefile` WRITE;
/*!40000 ALTER TABLE `eve_imagefile` DISABLE KEYS */;
INSERT INTO `eve_imagefile` VALUES (1,'image/2015/08/19/Penguins.jpg'),(2,'image/2015/08/19/Chrysanthemum.jpg'),(3,'image/2015/08/19/Desert.jpg'),(4,'image/2015/08/19/Hydrangeas.jpg'),(5,'image/2015/08/19/Jellyfish.jpg'),(6,'image/2015/08/19/Koala.jpg'),(7,'image/2015/08/18/startpicture_cZvfJui.jpg'),(8,'image/2015/08/20/startpicture.jpg'),(9,'image/2015/08/20/endpicture.jpg'),(10,'image/2015/08/20/endpicture_nXFpImb.jpg'),(11,'image/2015/08/20/endpicture_UtxRBiY.jpg'),(12,'image/2015/08/20/endpicture_rtg1tZE.jpg'),(13,'image/2015/08/20/endpicture_gh6ciUx.jpg'),(14,'image/2015/08/20/endpicture_HNe0ScT.jpg'),(15,'image/2015/08/20/endpicture_XZpb4Bn.jpg'),(16,'image/2015/08/20/endpicture_a1Z2yx3.jpg'),(17,'image/2015/08/20/endpicture_YNVQUxt.jpg'),(18,'image/2015/08/20/endpicture_ZxjIE9w.jpg'),(19,'image/2015/08/20/endpicture_a9YI7kj.jpg'),(20,'image/2015/08/20/endpicture_RP7zL3U.jpg'),(21,'image/2015/08/20/startpicture_mdSh8xs.jpg'),(22,'image/2015/08/20/endpicture_jcko8mw.jpg'),(23,'image/2015/08/20/endpicture_DvO71JK.jpg'),(24,'image/2015/08/20/endpicture_vqj75i6.jpg'),(25,'image/2015/08/20/endpicture_BvcF1rq.jpg'),(26,'image/2015/08/20/endpicture_QwiTwaS.jpg'),(27,'image/2015/08/20/endpicture_y9mzHCu.jpg'),(28,'image/2015/08/20/endpicture_2rltGRS.jpg'),(29,'image/2015/08/20/endpicture_VKMBmHw.jpg'),(30,'image/2015/08/20/endpicture_jsibs7Y.jpg'),(31,'image/2015/08/20/endpicture_Bf8VgpU.jpg'),(32,'image/2015/08/20/endpicture_jgDweJw.jpg'),(33,'image/2015/08/20/endpicture_TfCihN3.jpg'),(34,'image/2015/08/20/endpicture_0VhkUQg.jpg'),(35,'image/2015/08/20/endpicture_bhztMR8.jpg'),(36,'image/2015/08/20/endpicture_refAj4r.jpg'),(37,'image/2015/08/20/endpicture_jdF9h36.jpg'),(38,'image/2015/08/20/endpicture_JDCrqpX.jpg'),(39,'image/2015/08/20/endpicture_QffMOwU.jpg'),(40,'image/2015/08/20/endpicture_8HYryt8.jpg'),(41,'image/2015/08/20/endpicture_WLNPzQL.jpg'),(42,'image/2015/08/20/endpicture_JnWdAJE.jpg'),(43,'image/2015/08/20/endpicture_yzNvKKp.jpg'),(44,'image/2015/08/20/endpicture_NCNXj83.jpg'),(45,'image/2015/08/20/endpicture_tjEnXLt.jpg'),(46,'image/2015/08/20/endpicture_9TbDNeL.jpg'),(47,'image/2015/08/20/endpicture_7Ta4wNR.jpg'),(48,'image/2015/08/20/endpicture_89nZSpv.jpg'),(49,'image/2015/08/20/endpicture_F9Zp47g.jpg'),(50,'image/2015/08/20/endpicture_55zMNuH.jpg'),(51,'image/2015/08/20/endpicture_C1Lpxuo.jpg'),(52,'image/2015/08/20/endpicture_fHMJfo7.jpg'),(53,'image/2015/08/20/endpicture_RLgXZsU.jpg'),(54,'image/2015/08/20/endpicture_TYi7axS.jpg'),(55,'image/2015/08/20/endpicture_hwG3mTd.jpg'),(56,'image/2015/08/20/endpicture_oHwDQMd.jpg'),(57,'image/2015/08/20/endpicture_3QPJ5ol.jpg'),(58,'image/2015/08/20/endpicture_uJ2UtjC.jpg'),(59,'image/2015/08/20/endpicture_sYcyCIU.jpg'),(60,'image/2015/08/20/endpicture_vDqGZso.jpg'),(61,'image/2015/08/20/endpicture_XbZp2Hc.jpg'),(62,'image/2015/08/20/endpicture_jdGkfzG.jpg'),(63,'image/2015/08/20/endpicture_USYdZGp.jpg'),(64,'image/2015/08/20/endpicture_PVD2ADx.jpg'),(65,'image/2015/08/20/endpicture_mTVGdPb.jpg'),(66,'image/2015/08/20/endpicture_pdZMceA.jpg'),(67,'image/2015/08/20/endpicture_uxGehDJ.jpg'),(68,'image/2015/08/20/endpicture_Yt5hP8c.jpg'),(69,'image/2015/08/20/endpicture_lSnfwnC.jpg'),(70,'image/2015/08/20/endpicture_eirJJPG.jpg'),(71,'image/2015/08/20/endpicture_x9KuUYL.jpg'),(72,'image/2015/08/20/endpicture_pVgjTkc.jpg'),(73,'image/2015/08/20/endpicture_1a6L5tO.jpg'),(74,'image/2015/08/20/endpicture_7Ln5SMo.jpg'),(75,'image/2015/08/20/endpicture_l6yE61R.jpg'),(76,'image/2015/08/20/endpicture_DFhx6oG.jpg'),(77,'image/2015/08/20/endpicture_WK39Bs5.jpg'),(78,'image/2015/08/20/endpicture_25EbORX.jpg'),(79,'image/2015/08/20/endpicture_Eelsuop.jpg'),(80,'image/2015/08/20/endpicture_pc580vt.jpg'),(81,'image/2015/08/20/endpicture_A1uIm2H.jpg'),(82,'image/2015/08/20/endpicture_15wmS4J.jpg'),(83,'image/2015/08/20/endpicture_t19k0wK.jpg'),(84,'image/2015/08/20/endpicture_GC5vEpX.jpg'),(85,'image/2015/08/20/endpicture_fIT8YCS.jpg'),(86,'image/2015/08/20/endpicture_qCPY7vz.jpg'),(87,'image/2015/08/20/endpicture_6pfuv8i.jpg'),(88,'image/2015/08/20/endpicture_N1xadfY.jpg'),(89,'image/2015/08/20/endpicture_3vpqFr1.jpg'),(90,'image/2015/08/20/endpicture_JlgYujA.jpg'),(91,'image/2015/08/20/endpicture_xERQwre.jpg'),(92,'image/2015/08/20/endpicture_bPqsXcu.jpg'),(93,'image/2015/08/20/endpicture_D5XJJ5E.jpg'),(94,'image/2015/08/20/endpicture_ElZnU4M.jpg'),(95,'image/2015/08/20/endpicture_RXmyvBp.jpg'),(96,'image/2015/08/20/endpicture_IgKXMwz.jpg'),(97,'image/2015/08/20/endpicture_8kuUfo4.jpg'),(98,'image/2015/08/20/endpicture_cMuoE1k.jpg'),(99,'image/2015/08/20/endpicture_gvHxbpV.jpg'),(100,'image/2015/08/20/endpicture_zFP4bmZ.jpg'),(101,'image/2015/08/20/endpicture_JviEiaZ.jpg'),(102,'image/2015/08/20/endpicture_kpGYbVB.jpg'),(103,'image/2015/08/20/endpicture_NqNjTTH.jpg'),(104,'image/2015/08/20/startpicture_SwJaCLw.jpg'),(105,'image/2015/08/20/endpicture_XaAh3om.jpg'),(106,'image/2015/08/20/startpicture_CHpJ58l.jpg'),(107,'image/2015/08/20/endpicture_ou0HCTm.jpg'),(108,'image/2015/08/20/endpicture_L5sqT7W.jpg'),(109,'image/2015/08/20/endpicture_dQKYGkH.jpg'),(110,'image/2015/08/20/startpicture_8znIHAi.jpg'),(111,'image/2015/08/20/endpicture_Xv8JHTd.jpg'),(112,'image/2015/08/20/startpicture_wYMjLeQ.jpg'),(113,'image/2015/08/20/endpicture_bfacTpT.jpg'),(114,'image/2015/08/20/startpicture_wK5CI2J.jpg'),(115,'image/2015/08/20/endpicture_hS2iR7k.jpg'),(116,'image/2015/08/20/startpicture_wzrgG9w.jpg'),(117,'image/2015/08/20/startpicture_r0IMxzQ.jpg'),(118,'image/2015/08/20/endpicture_QZ0zDOE.jpg'),(119,'image/2015/08/20/startpicture_bxRuwzD.jpg'),(120,'image/2015/08/20/endpicture_HmsMXvx.jpg'),(121,'image/2015/08/20/startpicture_ZEEwGuI.jpg'),(122,'image/2015/08/20/endpicture_E0dh1KW.jpg'),(123,'image/2015/08/20/startpicture_NmqGZRx.jpg'),(124,'image/2015/08/20/startpicture_HW4drW5.jpg'),(125,'image/2015/08/20/endpicture_oQbW9FJ.jpg'),(126,'image/2015/08/20/startpicture_cZzsJR9.jpg'),(127,'image/2015/08/20/endpicture_46h2FvU.jpg'),(128,'image/2015/08/20/startpicture_86JCz18.jpg'),(129,'image/2015/08/20/endpicture_xACbkyP.jpg'),(130,'image/2015/08/20/startpicture_xvdsMoC.jpg'),(131,'image/2015/08/20/endpicture_oQ396eP.jpg'),(132,'image/2015/08/20/startpicture_Xg4OC0V.jpg'),(133,'image/2015/08/20/endpicture_niVhKS0.jpg'),(134,'image/2015/08/20/startpicture_zyirhZR.jpg'),(135,'image/2015/08/20/endpicture_yl68Jqa.jpg'),(136,'image/2015/08/20/startpicture_h3sB9mr.jpg'),(137,'image/2015/08/20/endpicture_13SseQb.jpg'),(138,'image/2015/08/20/startpicture_KWZTu8N.jpg'),(139,'image/2015/08/20/endpicture_K09Uojs.jpg'),(140,'image/2015/08/20/startpicture_YZEKulo.jpg'),(141,'image/2015/08/20/endpicture_SuziMwy.jpg'),(142,'image/2015/08/20/startpicture_L7YhTbl.jpg'),(143,'image/2015/08/20/endpicture_YQuHLPu.jpg'),(144,'image/2015/08/20/startpicture_h6hXY3M.jpg'),(145,'image/2015/08/20/endpicture_OjkiFJ0.jpg'),(146,'image/2015/08/20/startpicture_pZomUmb.jpg'),(147,'image/2015/08/20/startpicture_JBMgfmk.jpg'),(148,'image/2015/08/20/startpicture_vRaQISV.jpg'),(149,'image/2015/08/20/startpicture_8TZiIPR.jpg'),(150,'image/2015/08/20/startpicture_3i0GYw8.jpg'),(151,'image/2015/08/20/endpicture_ueeP1XX.jpg'),(152,'image/2015/08/20/startpicture_NZ2Khew.jpg'),(153,'image/2015/08/20/endpicture_rhfAfCb.jpg'),(154,'image/2015/08/20/startpicture_Ktwnh9x.jpg'),(155,'image/2015/08/20/startpicture_MtLvSHx.jpg'),(156,'image/2015/08/20/endpicture_Jhh6ixA.jpg'),(157,'image/2015/08/20/startpicture_8WhsEUk.jpg'),(158,'image/2015/08/20/endpicture_TGwO5bX.jpg'),(159,'image/2015/08/20/startpicture_dPoci2n.jpg'),(160,'image/2015/08/20/endpicture_fKIsbld.jpg'),(161,'image/2015/08/20/startpicture_TpfqhoH.jpg'),(162,'image/2015/08/20/endpicture_enBxCQb.jpg'),(163,'image/2015/08/20/startpicture_94PGlbN.jpg'),(164,'image/2015/08/20/endpicture_Ls7F8Mo.jpg'),(165,'image/2015/08/20/startpicture_VM5DRJw.jpg'),(166,'image/2015/08/20/endpicture_u3Tw0zs.jpg'),(167,'image/2015/08/20/startpicture_p08la2K.jpg'),(168,'image/2015/08/20/endpicture_v8arYJL.jpg'),(169,'image/2015/08/20/startpicture_GWSqFGU.jpg'),(170,'image/2015/08/20/endpicture_Rau8Szn.jpg'),(171,'image/2015/08/20/startpicture_o8Cc1hV.jpg'),(172,'image/2015/08/20/endpicture_qsYgotf.jpg'),(173,'image/2015/08/20/startpicture_oFdalVY.jpg'),(174,'image/2015/08/20/endpicture_hPrlhIo.jpg'),(175,'image/2015/08/20/startpicture_zgEudON.jpg'),(176,'image/2015/08/20/endpicture_jhNzD8D.jpg'),(177,'image/2015/08/20/startpicture_wD4Axwf.jpg'),(178,'image/2015/08/20/endpicture_EwR6GSE.jpg'),(179,'image/2015/08/20/startpicture_zabB7SE.jpg'),(180,'image/2015/08/20/startpicture_dTngqn2.jpg'),(181,'image/2015/08/20/endpicture_PqmN0C0.jpg'),(182,'image/2015/08/20/startpicture_dhPn7wy.jpg'),(183,'image/2015/08/20/endpicture_ax4Wkbp.jpg'),(184,'image/2015/08/20/startpicture_wh0Qc6V.jpg'),(185,'image/2015/08/20/startpicture_7D7rcw5.jpg'),(186,'image/2015/08/20/endpicture_JrZCsN1.jpg'),(187,'image/2015/08/20/startpicture_kBsyUDy.jpg'),(188,'image/2015/08/20/endpicture_lXqQoDA.jpg'),(189,'image/2015/08/20/startpicture_Q1UC6N5.jpg'),(190,'image/2015/08/20/startpicture_Qr2HktM.jpg'),(191,'image/2015/08/20/startpicture_Ju3lx9Z.jpg'),(192,'image/2015/08/20/endpicture_TZYp8qm.jpg'),(193,'image/2015/08/20/startpicture_cGyKm8P.jpg'),(194,'image/2015/08/20/startpicture_GOdoXo7.jpg'),(195,'image/2015/08/20/startpicture_Urca9Y4.jpg'),(196,'image/2015/08/20/endpicture_qDuDepO.jpg'),(197,'image/2015/08/20/startpicture_KYyQKym.jpg'),(198,'image/2015/08/20/endpicture_45S7Bgh.jpg'),(199,'image/2015/08/20/startpicture_ErfYumH.jpg'),(200,'image/2015/08/20/endpicture_bqe7yGi.jpg'),(201,'image/2015/08/20/startpicture_QqsLWai.jpg'),(202,'image/2015/08/20/startpicture_cPNEw6b.jpg'),(203,'image/2015/08/20/startpicture_p7ydCNc.jpg'),(204,'image/2015/08/20/startpicture_8nL0G1p.jpg'),(205,'image/2015/08/20/endpicture_lEmTOSz.jpg'),(206,'image/2015/08/20/startpicture_PjybYIe.jpg'),(207,'image/2015/08/20/endpicture_xgxs9vE.jpg'),(208,'image/2015/08/20/startpicture_BOkDAtw.jpg'),(209,'image/2015/08/20/endpicture_Bqpu6G3.jpg'),(210,'image/2015/08/20/startpicture_RENGbDY.jpg'),(211,'image/2015/08/20/endpicture_sW29Y6R.jpg'),(212,'image/2015/08/20/startpicture_yZ6LERl.jpg'),(213,'image/2015/08/20/endpicture_ULTTUOS.jpg'),(214,'image/2015/08/20/startpicture_QqMAoGZ.jpg'),(215,'image/2015/08/20/endpicture_L3SvvNR.jpg'),(216,'image/2015/08/20/startpicture_h8QA6Fs.jpg'),(217,'image/2015/08/20/startpicture_zbjaoFx.jpg'),(218,'image/2015/08/20/endpicture_f8r3sqN.jpg'),(219,'image/2015/08/20/startpicture_uLG9Hsy.jpg'),(220,'image/2015/08/20/endpicture_P4yKhHd.jpg'),(221,'image/2015/08/20/startpicture_bgsjkCj.jpg'),(222,'image/2015/08/20/endpicture_83tDqRt.jpg'),(223,'image/2015/08/20/startpicture_2Nl2JyW.jpg'),(224,'image/2015/08/20/endpicture_ogjORjW.jpg'),(225,'image/2015/08/20/startpicture_Nlxc1eR.jpg'),(226,'image/2015/08/20/endpicture_z3czkEL.jpg'),(227,'image/2015/08/20/startpicture_jv0hB4P.jpg'),(228,'image/2015/08/20/endpicture_gaaMWJ7.jpg'),(229,'image/2015/08/20/startpicture_H31GuVl.jpg'),(230,'image/2015/08/20/endpicture_BuLv4IY.jpg'),(231,'image/2015/08/20/startpicture_i1LYdyW.jpg'),(232,'image/2015/08/20/endpicture_cE6ftYY.jpg'),(233,'image/2015/08/20/startpicture_qVmmdn9.jpg'),(234,'image/2015/08/20/endpicture_delTfcZ.jpg'),(235,'image/2015/08/20/startpicture_692vuC7.jpg'),(236,'image/2015/08/20/startpicture_H60aA56.jpg'),(237,'image/2015/08/20/endpicture_zhQyevW.jpg'),(238,'image/2015/08/20/startpicture_RDDl02l.jpg'),(239,'image/2015/08/20/endpicture_RXlekZ8.jpg'),(240,'image/2015/08/20/startpicture_kU9NWtS.jpg'),(241,'image/2015/08/20/endpicture_JGNyfGx.jpg'),(242,'image/2015/08/20/startpicture_i3Rg66B.jpg'),(243,'image/2015/08/20/endpicture_crdrxdm.jpg'),(244,'image/2015/08/20/startpicture_82QjSkC.jpg'),(245,'image/2015/08/20/endpicture_isdgPby.jpg'),(246,'image/2015/08/20/startpicture_zlSkzkC.jpg'),(247,'image/2015/08/20/endpicture_MKzh8hp.jpg'),(248,'image/2015/08/20/startpicture_3SiuIsW.jpg'),(249,'image/2015/08/20/endpicture_7P8wawg.jpg'),(250,'image/2015/08/20/startpicture_GehgwJQ.jpg'),(251,'image/2015/08/20/endpicture_bzoucbP.jpg'),(252,'image/2015/08/20/startpicture_xEZe7vZ.jpg'),(253,'image/2015/08/20/endpicture_eNXYUqI.jpg'),(254,'image/2015/08/20/startpicture_3ion2Pd.jpg'),(255,'image/2015/08/20/endpicture_GRrRw8v.jpg'),(256,'image/2015/08/20/startpicture_QJRCiwB.jpg'),(257,'image/2015/08/20/endpicture_OcBvGrm.jpg'),(258,'image/2015/08/20/startpicture_0e1F1IZ.jpg'),(259,'image/2015/08/20/endpicture_1UszrZG.jpg'),(260,'image/2015/08/20/startpicture_7VapNdq.jpg'),(261,'image/2015/08/20/endpicture_h5HYbHC.jpg'),(262,'image/2015/08/20/startpicture_0qvFeiO.jpg'),(263,'image/2015/08/20/endpicture_4tMmccY.jpg'),(264,'image/2015/08/20/startpicture_Ya4Xg7N.jpg'),(265,'image/2015/08/20/endpicture_3vXAALB.jpg'),(266,'image/2015/08/20/startpicture_RzTON6c.jpg'),(267,'image/2015/08/20/endpicture_rt0rLEP.jpg'),(268,'image/2015/08/20/startpicture_YGW0V3T.jpg'),(269,'image/2015/08/20/endpicture_6kcVwbH.jpg'),(270,'image/2015/08/20/startpicture_0VBcKmM.jpg'),(271,'image/2015/08/20/endpicture_QTfruDt.jpg'),(272,'image/2015/08/20/startpicture_GIaOZdm.jpg'),(273,'image/2015/08/20/endpicture_6odQAH2.jpg'),(274,'image/2015/08/20/startpicture_suQ4sn6.jpg'),(275,'image/2015/08/20/endpicture_T3NbLHs.jpg'),(276,'image/2015/08/20/startpicture_olhGy1V.jpg'),(277,'image/2015/08/20/endpicture_ih7CJbK.jpg'),(278,'image/2015/08/20/startpicture_OCxzFlR.jpg'),(279,'image/2015/08/20/endpicture_BPFGiqB.jpg'),(280,'image/2015/08/20/startpicture_og10MKJ.jpg'),(281,'image/2015/08/20/endpicture_L7w1YnZ.jpg'),(282,'image/2015/08/20/startpicture_5U9jvTa.jpg'),(283,'image/2015/08/20/endpicture_GXuatV5.jpg'),(284,'image/2015/08/20/startpicture_OVwyWzk.jpg'),(285,'image/2015/08/20/endpicture_m6x0Uzu.jpg'),(286,'image/2015/08/20/startpicture_UvPpYM8.jpg'),(287,'image/2015/08/20/endpicture_AKKVJLB.jpg'),(288,'image/2015/08/20/startpicture_neReNIG.jpg'),(289,'image/2015/08/20/endpicture_wN3Cp41.jpg'),(290,'image/2015/08/20/startpicture_lItA4kK.jpg'),(291,'image/2015/08/20/endpicture_Ga2uDgR.jpg'),(292,'image/2015/08/20/startpicture_bYKjybJ.jpg'),(293,'image/2015/08/20/endpicture_ll50ZWK.jpg'),(294,'image/2015/08/20/startpicture_dGVoNb6.jpg'),(295,'image/2015/08/21/KakaoTalk_20150820_235509653.jpg'),(296,'image/2015/08/21/KakaoTalk_20150820_235509717.jpg'),(297,'image/2015/08/21/KakaoTalk_20150820_235510742.jpg'),(298,'image/2015/08/21/KakaoTalk_20150820_235511063.jpg'),(299,'image/2015/08/21/KakaoTalk_20150820_235512139.jpg'),(300,'image/2015/08/21/KakaoTalk_20150820_235512264.jpg'),(301,'image/2015/08/21/KakaoTalk_20150820_235513831.jpg'),(302,'image/2015/08/21/KakaoTalk_20150820_235514004.jpg'),(303,'image/2015/08/21/KakaoTalk_20150820_235515517.jpg'),(304,'image/2015/08/21/KakaoTalk_20150820_235515968.jpg'),(305,'image/2015/08/21/KakaoTalk_20150820_235516971.jpg'),(306,'image/2015/08/21/KakaoTalk_20150820_235517383.jpg'),(307,'image/2015/08/21/KakaoTalk_20150820_235517749.jpg'),(308,'image/2015/08/21/KakaoTalk_20150820_235518615.jpg'),(309,'image/2015/08/21/KakaoTalk_20150820_235518917.jpg'),(310,'image/2015/08/21/KakaoTalk_20150820_235519995.jpg'),(311,'image/2015/08/21/KakaoTalk_20150820_235521202.jpg'),(312,'image/2015/08/21/KakaoTalk_20150820_235521931.jpg'),(313,'image/2015/08/21/KakaoTalk_20150820_235522763.jpg'),(314,'image/2015/08/21/KakaoTalk_20150820_235509653_4ywF5CD.jpg'),(315,'image/2015/08/21/1.jpg'),(316,'image/2015/08/21/2.jpg'),(317,'image/2015/08/21/3.jpg'),(318,'image/2015/08/21/4.jpg'),(319,'image/2015/08/21/5.jpg'),(320,'image/2015/08/21/6.jpg'),(321,'image/2015/08/21/7.jpg'),(322,'image/2015/08/21/8.jpg'),(323,'image/2015/08/21/9.jpg'),(324,'image/2015/08/21/10.jpg'),(325,'image/2015/08/21/11.jpg'),(326,'image/2015/08/21/12.jpg'),(327,'image/2015/08/21/13.jpg'),(328,'image/2015/08/21/14.jpg'),(329,'image/2015/08/21/15.jpg'),(330,'image/2015/08/21/16.jpg'),(331,'image/2015/08/21/17.jpg'),(332,'image/2015/08/21/18.jpg'),(333,'image/2015/08/21/19.jpg'),(334,'image/2015/08/21/1_snyU2h5.jpg');
/*!40000 ALTER TABLE `eve_imagefile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `eve_user`
--

DROP TABLE IF EXISTS `eve_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `eve_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` longtext NOT NULL,
  `city` longtext NOT NULL,
  `address` longtext NOT NULL,
  `token` longtext NOT NULL,
  `device_id` int(11) DEFAULT NULL,
  `emergency_num` longtext,
  `msg` longtext,
  `picture_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `eve_user_9379346c` (`device_id`),
  KEY `eve_user_c6fe9aa6` (`picture_id`),
  CONSTRAINT `eve_user_picture_id_4d61b4c5d4b3f283_fk_eve_imagefile_id` FOREIGN KEY (`picture_id`) REFERENCES `eve_imagefile` (`id`),
  CONSTRAINT `eve_user_device_id_1f31f00bc2292e3e_fk_gcm_device_id` FOREIGN KEY (`device_id`) REFERENCES `gcm_device` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `eve_user`
--

LOCK TABLES `eve_user` WRITE;
/*!40000 ALTER TABLE `eve_user` DISABLE KEYS */;
INSERT INTO `eve_user` VALUES (1,'Dakyung','Seoul','Seoul, Kangnamku, Dokok1','8gAkj5z5lf',1,'01088471290/','Help me',1);
/*!40000 ALTER TABLE `eve_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `gcm_device`
--

DROP TABLE IF EXISTS `gcm_device`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `gcm_device` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `dev_id` varchar(50) NOT NULL,
  `reg_id` varchar(255) NOT NULL,
  `name` varchar(255) DEFAULT NULL,
  `creation_date` datetime NOT NULL,
  `modified_date` datetime NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `dev_id` (`dev_id`),
  UNIQUE KEY `reg_id` (`reg_id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `gcm_device`
--

LOCK TABLES `gcm_device` WRITE;
/*!40000 ALTER TABLE `gcm_device` DISABLE KEYS */;
INSERT INTO `gcm_device` VALUES (1,'APA91bGzX3IHxPX7U8ni_zgfOVswr7PcYyvg4HR6JJKV-fCEfx','APA91bGzX3IHxPX7U8ni_zgfOVswr7PcYyvg4HR6JJKV-fCEfxH6l3bhEyCpU8jA73KrlV5reoPY5QZAcf09wuIGZvGn3xpg69mdWV7jWB1HuLlx_4iYlV4','1','2015-08-19 21:09:06','2015-08-21 00:50:39',0);
/*!40000 ALTER TABLE `gcm_device` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-08-21  0:52:11
