from django.http import HttpResponse
from django.views.decorators.csrf import csrf_exempt
from django.shortcuts import render

from string import lowercase
from string import uppercase
from string import digits
from random import choice
from eve.models import *
from gcm.models import get_device_model, Device
from cdp.settings import MEDIA_URL

from datetime import datetime, timedelta
import json

def make_abs_path(req, rel_path):
    return req.build_absolute_uri('/') + MEDIA_URL.replace('/', '') + '/' + rel_path

def make_token():
    return ''.join(choice(lowercase + uppercase + digits) for i in xrange(10))

def get_failed_res(msg):
    res = {}

    res['success'] = False
    res['err_msg'] = msg
    return HttpResponse(json.dumps(res), content_type="application/json")

@csrf_exempt
def test(request):
    res = {}
    data = {}

    res['success'] = True
    res['data'] = data
    return HttpResponse(json.dumps(res), content_type="application/json")

@csrf_exempt
def join(request):
    res = {}
    data = {}

    name = request.POST.get('name')
    city = request.POST.get('city')
    address = request.POST.get('address')

    if name is None or city is None or address is None:
        return get_failed_res("lack of params")

    user = User(name=name, city=city, address=address, token=make_token())

    data['name'] = user.name
    data['city'] = user.city
    data['address'] = user.address
    data['token'] = user.token

    res['success'] = True
    res['data'] = data

    user.save()
    return HttpResponse(json.dumps(res), content_type="application/json")

@csrf_exempt
def start_alarm(request):
    today = datetime.today()
    token = request.META.get('HTTP_TOKEN')
    img_idx = request.POST.get('img_idx')

    if token is None or img_idx is None:
        return get_failed_res("lack of params")

    users = User.objects.filter(token=token)
    imgs = ImageFile.objects.filter(id=img_idx)

    if users.count() is 0:
        return get_failed_res("there are no matched users")

    user = users[0]

    if user.device is None:
        return get_failed_res("there are no linked device")

    if imgs.count() is 0:
        return get_failed_res("there are no matched imgs")
    img = imgs[0]

    alarm = Alarm(user=user, first_img=img, start_time=today, stat=0)
    alarm.save()

    prev = today - timedelta(days=7)
    alarms = Alarm.objects.filter(end_time__gt=prev, stat=1)

    msg = {}
    msg['cnt_alarms'] = alarms.count()
    msg['alarm_idx'] = alarm.id
    msg['level'] = 0
    msg['img'] = make_abs_path(request, str(img.img))

    user.device.send_message(msg)

    res = {}
    data = {}

    data['alarm_idx'] = alarm.id
    res['success'] = True
    res['data'] = data
    return HttpResponse(json.dumps(res), content_type="application/json")

@csrf_exempt
def end_alarm(request):
    today = datetime.today()

    token = request.META.get('HTTP_TOKEN')
    alarm_idx = request.POST.get('alarm_idx')
    is_vibrate = request.POST.get('is_vibrate')
    level = request.POST.get('level')
    img_idx = request.POST.get('img_idx')

    if level is None or img_idx is None:
        return get_failed_res("lack of params")

    if alarm_idx is None:
        users = User.objects.filter(token=token)
        imgs = ImageFile.objects.filter(id=img_idx);
        alarm = Alarm(user=users[0], first_img=imgs[0], start_time=today, level=5)
        alarm.save()
        alarm_idx = alarm.id

    alarm_idx = int(alarm_idx)
    level = int(level)

    alarms = Alarm.objects.filter(id=alarm_idx)
    imgs = ImageFile.objects.filter(id=img_idx)

    if level < 1 or level > 5:
        return get_failed_res("1 <= level <= 5")

    if alarms.count() is 0:
        return get_failed_res("there are no matched alarms")

    if imgs.count() is 0:
        return get_failed_res("there are no matched imgs")

    img = imgs[0]
    alarm = alarms[0]
    alarm.last_img = img

    if alarm.stat == 0:
        alarm.stat = 1

        prev = today - timedelta(days=7)

        msg = {}
        msg['cnt_alarms'] = Alarm.objects.filter(end_time__gt=prev, stat=1).count() + 1
        msg['alarm_idx'] = alarm.id
        msg['level'] = level
        msg['img'] = make_abs_path(request, str(img.img))

        alarm.user.device.send_message(msg)

    alarm.level = level
    alarm.end_time = today
    if is_vibrate is not None:
        alarm.is_vibrate = int(is_vibrate)

    alarm.save()

    res = {}
    data = {}

    res['success'] = True
    res['data'] = data

    return HttpResponse(json.dumps(res), content_type="application/json")

@csrf_exempt
def ignore_alarm(request):
    res = {}
    data = {}

    token = request.META.get('HTTP_TOKEN')
    alarm_idx = request.POST.get('alarm_idx')

    if token is None or alarm_idx is None:
        return get_failed_res("lack of params")

    alarms = Alarm.objects.filter(id=alarm_idx)

    if alarms.count() == 0:
        return get_failed_res("there are no matched alarms")

    alarm = alarms[0]
    alarm.stat = 2
    alarm.save()

    res['success'] = True
    res['data'] = data
    return HttpResponse(json.dumps(res), content_type="application/json")

@csrf_exempt
def update_gcm(request):
    res = {}
    data = {}

    token = request.META.get('HTTP_TOKEN')
    gcm = request.POST.get('gcm')

    if token is None or gcm is None:
        return get_failed_res("lack of params")

    users = User.objects.filter(token=token)

    if users.count() is 0:
        return get_failed_res("there are no matched users")

    user = users[0]

    devices = Device.objects.filter(name=user.id)

    if devices.count() is 0:
        device = Device(dev_id=gcm, reg_id=gcm, name=user.id)
    else:
        device = devices[0]

    device.reg_id = gcm
    device.save()

    user.device = device
    user.save()

    data['gcm'] = gcm
    data['token'] = user.token

    res['success'] = True
    res['data'] = data
    return HttpResponse(json.dumps(res), content_type="application/json")

@csrf_exempt
def clear(request):
    res = {}
    data = {}

    prev = Alarm.objects.count()
    Alarm.objects.filter(id__gt=21).delete()

    data['cnt_deleted'] = prev - 20

    res['success'] = True
    res['data'] = data
    return HttpResponse(json.dumps(res), content_type="application/json")


@csrf_exempt
def update_user(request):
    res = {}
    data = {}

    token = request.META.get('HTTP_TOKEN')
    img_idx = request.POST.get('img_idx')
    name = request.POST.get('name')
    emergency_num = request.POST.get('emergency_num')
    msg = request.POST.get('msg')
    address = request.POST.get('address')

    if token is None or \
            name is None or \
            emergency_num is None or\
            msg is None or \
            address is None:
        return get_failed_res("lack of params")

    users = User.objects.filter(token=token)

    if users.count() is 0:
        return get_failed_res("there are no matched users")

    user = users[0]

    if img_idx is not None:
        pictures = ImageFile.objects.filter(id=img_idx)

        if pictures.count() is 0:
            return get_failed_res("there are no matched img")

        picture = pictures[0]

        user.picture = picture

    user.name = name
    user.emergency_num = emergency_num
    user.msg = msg
    user.address = address

    user.save()

    data['token'] = token
    data['name'] = name
    data['address'] = address
    data['msg'] = msg

    if user.emergency_num is not None:
        data['emergency_num'] = emergency_num

    if user.picture is not None:
        data['picture'] = make_abs_path(request, str(user.picture.img))

    res['success'] = True
    res['data'] = data
    return HttpResponse(json.dumps(res), content_type="application/json")

@csrf_exempt
def get_test_user(request):
    users = User.objects.filter(id=1)

    if users.count() is 0:
        return get_failed_res("there are no test user")

    user = users[0]

    res = {}
    data = {}

    data['token'] = user.token
    data['name'] = user.name
    data['city'] = user.city

    data['address'] = user.address

    if user.emergency_num is not None:
        data['emergency_num'] = user.emergency_num

    if user.msg is not None:
        data['msg'] = user.msg

    if user.device is not None:
        data['gcmId'] = user.device.dev_id

    if user.picture is not None:
        data['picture'] = make_abs_path(request, str(user.picture.img))

    res['success'] = True
    res['data'] = data
    return HttpResponse(json.dumps(res), content_type="application/json")

@csrf_exempt
def get_weekly_report(request):
    res = {}
    data = {}

    token = request.META.get('HTTP_TOKEN')
    users = User.objects.filter(token=token)

    if token is None:
        return get_failed_res("lack of params")

    if users.count() is 0:
        return get_failed_res('there are no matched users')

    user = users[0]

    today = datetime.today()

    prev = today - timedelta(days=7)

    als = Alarm.objects.filter(end_time__gt=prev, stat=1)

    medial_base_url = request.build_absolute_uri('/') + MEDIA_URL.replace('/', '') + '/'

    alarms = []
    for al in als:
        alarms.append(al.as_dict(medial_base_url))

    data['alarms'] = alarms
    res['success'] = True
    res['data'] = data
    return HttpResponse(json.dumps(res), content_type="application/json")

@csrf_exempt
def upload_img(request):
    if request.method == 'POST':

        token = request.META.get('HTTP_TOKEN')
        img = request.FILES.get("img")

        if token is None:
            return get_failed_res('there is no token')

        if User.objects.filter(token=token).count() == 0:
            return get_failed_res('there are no matched users')

        if img is None:
            return get_failed_res('there is no img file')

        img_file = ImageFile(img=img)
        img_file.save()

        res = {}
        data = {}

        data['img_url'] = make_abs_path(request, str(img_file.img))
        data['idx'] = img_file.id

        res['success'] = True
        res['data'] = data
        return HttpResponse(json.dumps(res), content_type="application/json")

