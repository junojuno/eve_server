# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eve', '0005_auto_20150804_0318'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='emergency_num',
            field=models.TextField(max_length=1000, null=True),
        ),
        migrations.AddField(
            model_name='user',
            name='msg',
            field=models.TextField(max_length=300, null=True),
        ),
    ]
