# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eve', '0006_auto_20150808_2355'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='picture',
            field=models.ForeignKey(to='eve.ImageFile', null=True),
        ),
    ]
