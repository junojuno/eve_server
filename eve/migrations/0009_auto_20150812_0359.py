# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eve', '0008_alarm_isvibrate'),
    ]

    operations = [
        migrations.RenameField(
            model_name='alarm',
            old_name='isVibrate',
            new_name='is_vibrate',
        ),
    ]
