# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eve', '0009_auto_20150812_0359'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='alarm',
            name='picture',
        ),
        migrations.AddField(
            model_name='alarm',
            name='first_img',
            field=models.ForeignKey(related_name='first_img', to='eve.ImageFile', null=True),
        ),
        migrations.AddField(
            model_name='alarm',
            name='last_img',
            field=models.ForeignKey(related_name='last_img', to='eve.ImageFile', null=True),
        ),
    ]
