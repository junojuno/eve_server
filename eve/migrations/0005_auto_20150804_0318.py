# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eve', '0004_user_device'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='alarm',
            name='datetime',
        ),
        migrations.AddField(
            model_name='alarm',
            name='end_time',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='alarm',
            name='start_time',
            field=models.DateTimeField(null=True),
        ),
        migrations.AddField(
            model_name='alarm',
            name='stat',
            field=models.IntegerField(default=0),
        ),
        migrations.AlterField(
            model_name='alarm',
            name='level',
            field=models.IntegerField(null=True),
        ),
    ]
