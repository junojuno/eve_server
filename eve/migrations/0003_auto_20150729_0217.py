# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eve', '0002_auto_20150729_0212'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='user',
            name='device',
        ),
        migrations.DeleteModel(
            name='Device',
        ),
    ]
