# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Alarm',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('level', models.IntegerField()),
                ('datetime', models.DateTimeField(auto_now_add=True)),
            ],
        ),
        migrations.CreateModel(
            name='ImageFile',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('img', models.FileField(upload_to=b'image/%Y/%m/%d')),
            ],
        ),
        migrations.CreateModel(
            name='User',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.TextField(max_length=50)),
                ('city', models.TextField(max_length=50)),
                ('address', models.TextField(max_length=200)),
                ('token', models.TextField(max_length=50)),
            ],
        ),
        migrations.AddField(
            model_name='alarm',
            name='picture',
            field=models.ForeignKey(to='eve.ImageFile'),
        ),
        migrations.AddField(
            model_name='alarm',
            name='user',
            field=models.ForeignKey(to='eve.User'),
        ),
    ]
