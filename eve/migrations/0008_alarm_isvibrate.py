# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('eve', '0007_user_picture'),
    ]

    operations = [
        migrations.AddField(
            model_name='alarm',
            name='isVibrate',
            field=models.IntegerField(default=0),
        ),
    ]
