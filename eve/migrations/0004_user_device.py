# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('gcm', '0001_initial'),
        ('eve', '0003_auto_20150729_0217'),
    ]

    operations = [
        migrations.AddField(
            model_name='user',
            name='device',
            field=models.ForeignKey(to='gcm.Device', null=True),
        ),
    ]
