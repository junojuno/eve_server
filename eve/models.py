from django.db import models
from gcm.models import AbstractDevice, Device


# class Device(AbstractDevice):
#     default_msg = models.TextField(max_length=100)

class ImageFile(models.Model):
    img = models.FileField(upload_to='image/%Y/%m/%d')

class User(models.Model):
    picture = models.ForeignKey(ImageFile, null=True)
    name = models.TextField(max_length=50)
    city = models.TextField(max_length=50)
    address = models.TextField(max_length=200)
    token = models.TextField(max_length=50)
    emergency_num = models.TextField(max_length=1000, null=True)
    msg = models.TextField(max_length=300, null=True)
    # device = models.ForeignKey(Device)
    device = models.ForeignKey(Device, null=True)


class Alarm(models.Model):
    user = models.ForeignKey(User)
    level = models.IntegerField(null=True)

    # first_picture
    first_img = models.ForeignKey(ImageFile, related_name='first_img', null=True)

    # last_picture
    last_img = models.ForeignKey(ImageFile, null=True, related_name='last_img')

    start_time = models.DateTimeField(null=True)
    end_time = models.DateTimeField(null=True)

    #stat {0 = isStarted, 1 = isEnded, 2 = disabled}
    stat = models.IntegerField(default=0)
    is_vibrate = models.IntegerField(default=0)

    def as_dict(self, media_base_url):
        return dict(
            level=self.level,
            picture=media_base_url + self.last_img.img.__str__(),
            end_time='%04d-%0'
                     '2d-%02d %02d:%02d:%02d' %
                     (self.end_time.year, self.end_time.month, self.end_time.day,
                      self.end_time.hour, self.end_time.minute, self.end_time.second),
            start_time='%04d-%02d-%02d %02d:%02d:%02d' %
                     (self.start_time.year, self.start_time.month, self.start_time.day,
                      self.start_time.hour, self.start_time.minute, self.start_time.second),
            diff=(self.end_time - self.start_time).seconds,
            is_vibrate=self.is_vibrate
        )
